import numpy as np
from sklearn.metrics import f1_score

from vectorizer import prepare


def majority_baseline(test_labels):
    """Calculates the majority baseline for binary accuracy.

    Args:
        test_labels (list of (dict of str:(list of float))): Test labels.
    
    Returns:
        float: Majority baseline accuracy on the test labels.

    """
    counts = np.sum(test_labels, axis=0)
    counts = np.maximum(counts, np.full(len(counts), len(test_labels)-counts))
    return np.sum(counts)/(len(counts)*len(test_labels))


def evaluate(model, test_samples, test_labels):
    """Evaluate a fitted Keras model.
        Prints binary accuracy, micro f1 score, macro f1 score.

    Args:
        model (`Sequential`): The model.
        test_samples (list of (list of (list of float))): Test samples.
        test_labels (list of (dict of str:(list of float))): Test labels.
    
    """
    test_samples, test_labels = exclude_nan_labels(test_samples, test_labels)
    model.evaluate(test_samples, test_labels)
    pred_labels = model.predict(test_samples)
    pred_labels = [[round(x) for x in outputs] for outputs in pred_labels]
    print("binary_majority:", majority_baseline(test_labels))
    print("micro_f1_score:", f1_score(test_labels, pred_labels, average="micro", zero_division=0))
    print("macro_f1_score:", f1_score(test_labels, pred_labels, average="macro", zero_division=0))


def exclude_nan_labels(test_samples, test_labels):
    """Removes samples without labels.

    Args:
        test_samples (list of (list of (list of float))): List of samples.
        test_labels (list of (dict of str:(list of float))): List of labels.
    
    Returns:
        list of (list of (list of float)): Reduced list of samples.
        list of (dict of str:(list of float)): Reduced list of labels.
    
    """
    test_samples, test_labels = tuple(map(list, zip(*[(test_sample, test_label) for test_sample, test_label in zip(test_samples, test_labels) if not np.isnan(test_label).any()])))
    return prepare(test_samples), prepare(test_labels)