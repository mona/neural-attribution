import gzip
import json
import numpy as np
import os
import pickle
import pickletools
import torch
import tqdm
from sklearn.preprocessing import MultiLabelBinarizer
from transformers import AutoTokenizer, AutoModelForTokenClassification
from transformers import BertModel, BertTokenizer


#TOKENIZER = BertTokenizer.from_pretrained("severinsimmler/literary-german-bert")
#MODEL = BertModel.from_pretrained("severinsimmler/literary-german-bert", output_hidden_states=True)
TOKENIZER = BertTokenizer.from_pretrained("dbmdz/bert-base-german-cased")
MODEL = BertModel.from_pretrained("dbmdz/bert-base-german-cased", output_hidden_states=True)


LABEL_NAMES = ["Figur", "Erzählinstanz", "Verdacht Autor"]
LABEL_ENCODER = MultiLabelBinarizer()
LABEL_ENCODER.fit([LABEL_NAMES])


def cuda(obj):
    """Add an object to GPU if available, else to CPU.

    Args:
        obj (obj): The object.
    
    Returns:
        obj: The object.
    
    """
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    obj = obj.to(device)
    return obj


def create_embeddings(string):
    """Create word embeddings for a string.

    Args:
        string (str): The string.
    
    Returns:
        list of (list of float): List of word embeddings, one for each word in the string.
        list of str: List of words in the string (as produced by the tokenizer).
    
    """
    model = cuda(MODEL)
    model = model.eval()
    string_ids = TOKENIZER.encode(string, add_special_tokens=False)
    tokens = TOKENIZER.convert_ids_to_tokens(string_ids)
    string_ids = torch.LongTensor(string_ids)
    string_ids = cuda(string_ids)
    string_ids = string_ids.unsqueeze(0)
    with torch.no_grad():
        out = model(input_ids=string_ids)
    hidden_states = out[2]
    embeddings = hidden_states[-1][0]
    embeddings = [[float(x) for x in embedding] for embedding in embeddings]
    return embeddings, tokens


def read_samples(name):
    """Read samples from `data`.

    Args:
        name (str): `train`, `dev` or `test`.
    
    Returns:
        list of (dict of str:obj): List of samples.
    
    """
    with open(os.path.join("data", name + "_clauses.json"), "r") as f:
        return json.load(f)


def vectorize_samples(samples):
    """Create clause-specific passage embeddings for a list of samples.
        Each sample (i.e. a passage) is thus represented by several vectors (for every clause in the passage).

    Args:
        samples (list of (dict of str:obj)): List of samples.
    
    Returns:
        list of (list of (list of float)): Clause-specific passage embeddings.
        list of (dict of str:(list of float)): Clause-specific multi-hot label encodings for each annotator.
    
    """
    clause_embeddings, clause_labels = [], []
    for sample in tqdm.tqdm(samples):
        sample_clause_embeddings, sample_clause_labels = vectorize_sample(sample)
        clause_embeddings.extend(sample_clause_embeddings)
        clause_labels.extend(sample_clause_labels)
    return clause_embeddings, clause_labels


def vectorize_sample(sample):
    """Create clause-specific passage embeddings for a sample.

    Args:
        sample (dict of str:obj): A sample.
    
    Returns:
        list of (list of (list of float)): For every clause in the passage, the word vectors for the passage with one additional dimension (at index 0).
            The additional dimension is 1 if the word belongs to the clause; 0 otherwise.
        list of (dict of str:(list of float)): For every clause in the passage, multi-hot label encodings for each annotator.
    
    """
    tokens = [token for _, token in sample["tokens"]]
    string = " ".join(tokens)
    embeddings, subtokens = create_embeddings(string)
    subtoken_to_token_mappings = map_subtokens_to_tokens(subtokens, tokens)
    subtoken_clauses = [sample["tokens"][i][0] for i in subtoken_to_token_mappings]
    clause_embeddings, clause_labels = create_clause_embeddings(subtoken_clauses, embeddings, sample["labels"])
    return clause_embeddings, clause_labels


def create_clause_embeddings(subtoken_clauses, embeddings, labels):
    """Create a passage embedding and the corresponding labels for every clause in the passage.

    Args:
        subtoken_clauses (list of int): Clause indices for each subtoken (each subtoken corresponds to one word vector).
        embeddings (list of (list of float)): Word vectors for the passage.
        labels (dict of str:(dict of str:(list of str))): A dictionary which maps a (stringified) clause index to a list of labels for each annotator.
    
    Returns:
        list of (list of (list of float)): For every clause in the passage, the word vectors for the passage with one additional dimension (at index 0).
            The additional dimension is 1 if the word belongs to the clause; 0 otherwise.
        list of (dict of str:(list of float)): For every clause in the passage, multi-hot label encodings for each annotator.
    
    """
    clause_embeddings = []
    clause_labels = []
    clause_indices = set(subtoken_clauses)
    clause_indices.discard(None)
    for clause in sorted(list(clause_indices)):
        try:
            clause_labels.append(transform_labels(labels[str(clause)]))
            clause_embeddings.append([[float(subtoken_clauses[i] == clause)] + embedding for i, embedding in enumerate(embeddings)])
        except KeyError: # rare case: clause without annotation
            pass
    return clause_embeddings, clause_labels


def transform_labels(labels):
    """Transform labels.

    Args:
        labels (dict of str:(list of str)): A list of labels for each annotator.
    
    Returns:
        dict of str:(list of float): Multi-hot label encodings for each annotator.
    
    """
    return {annotator : [float(x) for x in list(LABEL_ENCODER.transform([labels[annotator]])[0])] for annotator in labels}


def map_subtokens_to_tokens(subtokens, tokens):
    """Map subtokens (i.e. tokens produced by the BERT tokenizer) to tokens (as in the data).

    Args:
        subtokens (list of str): List of subtokens.
            Example: ['(', '›abheben‹', 'ist', 'übrigens', 'auch', 'trivial', ';', 'entschuldigen', 'Sie', ',', 'Rex', ')', '.']
        tokens (list of str): List of tokens.
            Example: ['(', '›', 'ab', '##heben', '[UNK]', 'ist', 'übrigens', 'auch', 'tri', '##via', '##l', ';', 'entschuldigen', 'Sie', ',', 'Re', '##x', ')', '.']
    
    Returns:
        list of int: For each subtoken the index of the corresponding token.
            Example: [0, 1, 1, 1, 1, 2, 3, 4, 5, 5, 5, 6, 7, 8, 9, 10, 10, 11, 12]
    
    """
    subtoken_to_token_mappings = []
    current_token_index = 0
    current_token_pos = 0
    for subtoken in subtokens:
        current_token = tokens[current_token_index]
        subtoken = subtoken.strip("#")
        if current_token[current_token_pos:].startswith(subtoken):
            subtoken_to_token_mappings.append(current_token_index)
            current_token_pos += len(subtoken)
        elif subtoken == "[UNK]": # "‹"
            subtoken_to_token_mappings.append(current_token_index)
            current_token_pos += 1
        else:
            raise ValueError("Subtokens cannot be mapped to tokens!")
        if current_token_pos == len(current_token):
            current_token_index += 1
            current_token_pos = 0
    return subtoken_to_token_mappings


def pad_samples(samples, max_len):
    """Pad embeddings to a given length. The embeddings are centered.

    Args:
        samples (list of (list of (list of float))): The embeddings, each a list of word vectors.
        max_len (int): The wanted size of the embeddings.
    
    Returns:
        list of (list of (list of float)): The padded versions of the embeddings.
    
    """
    return [pad_sample(sample, max_len) for sample in tqdm.tqdm(samples)]


def pad_sample(sample, max_len):
    """Pad an embedding to a given length. The embedding is centered.
        If it is too long, an equal number of word vectors is removed from the left and from the right.
        If it is too short, an equal number of zero vectors is inserted at the left and at the right.

    Args:
        sample (list of (list of float)): The embedding as a list of word vectors.
        max_len (int): The wanted size of the embedding.
    
    Returns:
        list of (list of float): The padded version of the embedding.
    
    """
    zero_vector = [0.0] * len(sample[0])
    diff = max_len-len(sample)
    if diff == 0:
        return sample
    d = int(abs(diff)/2.0)
    if diff > 0:
        return ([zero_vector] * (diff%2)) + ([zero_vector] * d) + sample + ([zero_vector] * d)
    if diff < 0:
        print(len(sample), len(sample[d+(diff%2):(-d if d > 0 else len(sample))]))
        return sample[d+(diff%2):(-d if d > 0 else len(sample))]


def gimme_samples():
    """Read, vectorize and pad train, dev and test samples.
        Pickles the vectors after first creation to load them in subsequent function calls.

    Returns:
        list of (list of (list of float)): Train samples.
        list of (list of (list of float)): Dev samples.
        list of (list of (list of float)): Test samples.
        list of (dict of str:(list of float)): Train labels.
        list of (dict of str:(list of float)): Dev labels.
        list of (dict of str:(list of float)): Test labels.
        int: Length of the padded samples.
    
    """
    if os.path.exists(os.path.join("data", "vec_clauses.pickle")):
        with gzip.open(os.path.join("data", "vec_clauses.pickle"), "rb") as f:
            return pickle.load(f)

    else:

        train_samples = read_samples("train")
        dev_samples = read_samples("dev")
        test_samples = read_samples("test")
        
        print("Vectorize samples")
        train_samples, train_labels = vectorize_samples(train_samples)
        dev_samples, dev_labels = vectorize_samples(dev_samples)
        test_samples, test_labels = vectorize_samples(test_samples)

        #max_len = max([len(sample) for sample in train_samples + dev_samples])
        max_len = 123

        print("Pad samples")
        train_samples = pad_samples(train_samples, max_len)
        dev_samples = pad_samples(dev_samples, max_len)
        test_samples = pad_samples(test_samples, max_len)

        with gzip.open(os.path.join("data", "vec_clauses.pickle"), "wb") as f:
            f.write(pickletools.optimize(pickle.dumps((train_samples, dev_samples, test_samples, train_labels, dev_labels, test_labels), protocol=-1)))

        return train_samples, dev_samples, test_samples, train_labels, dev_labels, test_labels


def prepare(X):
    """Transform an array-like object to Numpy format.

    Args:
        X (list of obj): Array-like object.
    
    Returns:
        np.array: Numpy array.
    
    """
    return np.asarray(X, dtype=np.float32)


def transform_data(samples, labels, feats=[]):
    """Transform samples and labels to input matrices for Keras models.

    Args:
        samples (list of (list of (list of float))): Samples.
        labels (list of (dict of str:(list of float))): Labels for each annotator.
    
    Returns:
        np.array: [sample1, sample2, ...]
            List of samples.
        np.array: [sample1, sample1, ..., sample2, sample2, ...]
            List of samples. Each sample is repeated by the number of annotators.
        dict of str:np.array: {annotatori : [sample1, sample1, ..., sample2, sample2, ...]}
            List of samples. Each sample is repeated by the number of annotators.
            The samples for the key annotator are missing.
        np.array: [sample2, ...]
            List of samples that are annotated identically by each annotator.
        list of str: [annotator1, annotator2, ..., annotator1, annotator2, ...]
            List of annotators. The list is repeated by the number of samples.
        dict of str:str: {annotatori : [annotator1, annotator2, ..., annotator1, annotator2, ...]}
            List of annotators. The list is repeated by the number of samples.
            The annotarors corresponding to the samples for the key annotator are missing.
        np.array: [anno1, anno2, ..., anno1, anno2, ...]
            List of annotator vectors. The list is repeated by the number of samples.
        dict of str:np.array: {annotatori : [anno1, anno2, ..., anno1, anno2, ...]}
            List of annotator vectors. The list is repeated by the number of samples.
            The annotator vectors corresponding to the samples for the key annotator are missing.
        list of np.array: [annoX, annoX, ..., annoX, annoX, ...]
            List of annotator vectors. The list is repeated by the number of samples.
            All samples correspond to special annotator vectors.
        list of np.array: [anno1, anno2, ..., annoX, annoX, ...]
            List of annotator vectors. The list is repeated by the number of samples.
            Samples that are annotated identically by each annotator correspond to special annotator vectors.
        list of np.array: [annoX, ...]
            List of special annotator vectors for all samples that are annotated identically by each annotator.
        list of np.array: [annoX, ...]
            List of special annotator vectors for all samples.
        dict of str:np.array: {annotatori : [annoi, annoi, ...]}
            Mappings from annotator to list of annotator vectors for each sample.
        np.array: [labels11, labels12, ...]
            List of labels for each sample-annotator pair.
        dict of str:np.array: {annotatori : [labels11, labels12, ...]}
            List of labels for each sample-annotator pair.
            The samples for the key annotator are missing.
        np.array: [labels1X, labels2X, ...]
            List of labels for all samples that are annotated identically by each annotator.
        dict of str:np.array: {annotatori : [labels1i, labels2i, ...]}
            Mappigns from annotator to list of labels for each sample.
    
    """
    samples_all = []
    samples_all_except = {}
    samples_same = []
    annotators_all = []
    annotators_all_except = {}
    annos_all = []
    annos_all_except = {}
    annos_all_special = []
    annos_all_special_same = []
    annos_same = []
    annos_special = []
    annos_anno = {}
    labels_all = []
    labels_all_except = {}
    labels_same = []
    labels_anno = {}

    annotators = sorted([annotator for annotator in set([label for labels_ in labels for label in labels_])])
    if not (feats is None or len(feats) > 0):
        one_hot_encodings = {}
        for i, annotator in enumerate(annotators):
            one_hot_encoding = [0] * len(annotators)
            one_hot_encoding[i] = 1
            one_hot_encodings[annotator] = one_hot_encoding
        none_hot_encoding = [0] * len(annotators)
    else:
        one_hot_encodings = read_annotator_data(feats)
        none_hot_encoding = None
        for annotator in annotators:
            one_hot_encoding = one_hot_encodings[annotator]
            none_hot_encoding = [0] * len(one_hot_encoding)
            break
    
    print("Transform data")
    for i, sample in enumerate(tqdm.tqdm(samples)):
        sample_annos = []
        sample_annos_special = []
        sample_labels = set()
        for annotator in annotators:
            try:
                label = labels[i][annotator]
            except KeyError:
                label = [None]*len(LABEL_NAMES)
            samples_all.append(sample)
            annotators_all.append(annotator)
            sample_annos.append(one_hot_encodings[annotator])
            sample_annos_special.append(none_hot_encoding)
            labels_all.append(label)
            sample_labels.add(tuple(label))
            try:
                annos_anno[annotator].append(one_hot_encodings[annotator])
                labels_anno[annotator].append(label)
            except KeyError:
                annos_anno[annotator] = [one_hot_encodings[annotator]]
                labels_anno[annotator] = [label]
            for annotator_ in labels[i]:
                if annotator_ != annotator:
                    try:
                        samples_all_except[annotator_].append(sample)
                        annotators_all_except[annotator_].append(annotator)
                        annos_all_except[annotator_].append(one_hot_encodings[annotator])
                        labels_all_except[annotator_].append(label)
                    except KeyError:
                        samples_all_except[annotator_] = [sample]
                        annotators_all_except[annotator_] = [annotator]
                        annos_all_except[annotator_] = [one_hot_encodings[annotator]]
                        labels_all_except[annotator_] = [label]
        annos_all.extend(sample_annos)
        annos_all_special.extend(sample_annos_special)
        annos_special.append(none_hot_encoding)
        if len(sample_labels) == 1:
            annos_all_special_same.extend([none_hot_encoding] * len(sample_annos))
            samples_same.append(sample)
            annos_same.append(none_hot_encoding)
            labels_same.append(list(sample_labels.pop()))
        else:
            annos_all_special_same.extend(sample_annos)
        
    return prepare(samples), prepare(samples_all), {annotator : prepare(samples_all_except[annotator]) for annotator in samples_all_except}, prepare(samples_same), annotators_all, annotators_all_except, prepare(annos_all), {annotator: prepare(annos_all_except[annotator]) for annotator in annos_all_except}, prepare(annos_all_special), prepare(annos_all_special_same), prepare(annos_same), prepare(annos_special), {annotator : prepare(annos_anno[annotator]) for annotator in annos_anno}, prepare(labels_all), {annotator : prepare(labels_all_except[annotator]) for annotator in labels_all_except}, prepare(labels_same), {annotator : prepare(labels_anno[annotator]) for annotator in labels_anno}


def read_annotator_data(feats=None):
    """Read the annotator data from `questionnaire.csv`.

    Args:
        feats (list of str): Names of the columns to read in.
            If None, all columns are read in.
    
    Returns:
        dict of str:(list of int): Mappings from annotators to annotator vectors.
    
    """
    annotator_data = {}
    with open(os.path.join(os.path.dirname(__file__), "annotators", "questionnaire.csv"), "r") as f:
        for i, line in enumerate(f.readlines()):
            line = line.strip()
            line = line.split(",")
            if i == 0:
                feat_names = line[1:]
            else:
                annotator_data[line[0]] = [float(x)-3.0 for k, x in enumerate(line[1:]) if feats is None or feat_names[k] in feats]
    return annotator_data


if __name__ == "__main__":
    train_samples, dev_samples, test_samples, train_labels, dev_labels, test_labels = gimme_samples()
    print(len(train_samples), len(dev_samples), len(test_samples))