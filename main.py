from numpy.random import seed
seed(42)
import tensorflow
tensorflow.random.set_seed(42)

import keras
import numpy as np
import os
from matplotlib import pyplot as plt
from nltk.metrics.agreement import AnnotationTask
from nltk.metrics import masi_distance
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.decomposition import TruncatedSVD
from tensorflow.keras.callbacks import EarlyStopping

from correlations import select_top_feats
from evaluation import evaluate, exclude_nan_labels
from models import *
from vectorizer import gimme_samples, transform_data


EPOCHS_MIN = 30
EPOCHS_MAX = 100
BATCH_SIZE = 32
VERBOSE = 1


def load_if_exists(name, model, fit_samples, fit_labels, val_samples, val_labels):
    path = os.path.join("models", name)
    if os.path.exists(path):
        model = keras.models.load_model(path)
    else:
        fit_samples, fit_labels = exclude_nan_labels(fit_samples, fit_labels)
        val_samples, val_labels = exclude_nan_labels(val_samples, val_labels)
        early_stopping = EarlyStopping(monitor="val_binary_accuracy", mode="max", min_delta=0, patience=EPOCHS_MIN, restore_best_weights=True)
        model.fit(fit_samples, fit_labels, epochs=EPOCHS_MAX, batch_size=BATCH_SIZE, verbose=VERBOSE, validation_data=(val_samples,val_labels), callbacks=[early_stopping])
        model.save(path)
    return model


def masi_distance_with_zero_division(label1, label2):
    try:
        return masi_distance(label1, label2)
    except ZeroDivisionError:
        return 0.0


if __name__ == "__main__":
    train_samples, dev_samples, test_samples, train_labels, dev_labels, test_labels = gimme_samples()

    feats = [] # one-hot features
    train_samples, train_samples_all, train_samples_all_except, train_samples_same, train_annotators_all, train_annotators_all_except, train_annos_all, train_annos_all_except, train_annos_all_special, train_annos_all_special_same, train_annos_same, train_annos_special, train_annos_anno, train_labels_all, train_labels_all_except, train_labels_same, train_labels_anno = transform_data(train_samples, train_labels, feats)
    dev_samples, dev_samples_all, dev_samples_all_except, dev_samples_same, dev_annotators_all, dev_annotators_all_except, dev_annos_all, dev_annos_all_except, dev_annos_all_special, dev_annos_all_special_same, dev_annos_same, dev_annos_special, dev_annos_anno, dev_labels_all, dev_labels_all_except, dev_labels_same, dev_labels_anno = transform_data(dev_samples, dev_labels, feats)
    test_samples, test_samples_all, test_samples_all_except, test_samples_same, test_annotators_all, test_annotators_all_except, test_annos_all, test_annos_all_except, test_annos_all_special, test_annos_all_special_same, test_annos_same, test_annos_special, test_annos_anno, test_labels_all, test_labels_all_except, test_labels_same, test_labels_anno = transform_data(test_samples, test_labels, feats)

    annotators = sorted(list(set(train_annotators_all)))

    def ka(a1, a2):
        x1 = train_labels_anno[annotators[int(a1[0])]]
        x2 = train_labels_anno[annotators[int(a2[0])]]
        x1 = [(annotators[int(a1[0])], str(i), frozenset([str(j) for j, v in enumerate(x) if v >= 0.5])) for i, x in enumerate(x1)]
        x2 = [(annotators[int(a2[0])], str(i), frozenset([str(j) for j, v in enumerate(x) if v >= 0.5])) for i, x in enumerate(x2)]
        task = AnnotationTask(distance=masi_distance_with_zero_division)
        task.load_array(x1+x2)
        return 1-max(0,task.alpha())
    
    # cluster annotators by annotations
    linked = linkage([[i] for i in range(len(annotators))], 'average', metric=ka)
    plt.figure(figsize=(10, 7))
    dendrogram(linked, orientation='top', labels=annotators, distance_sort='descending', color_threshold=0)
    plt.savefig("figs/fig0.png")

    # no encoding
    model = model_without_annotator()
    model = load_if_exists("no_encoding", model, train_samples_all, train_labels_all, dev_samples_all, dev_labels_all)
    print("no encoding", "aggregated", "train")
    evaluate(model, train_samples_all, train_labels_all)
    print("no encoding", "aggregated", "dev")
    evaluate(model, dev_samples_all, dev_labels_all)
    print("no encoding", "aggregated", "test")
    evaluate(model, test_samples_all, test_labels_all)
    for test_annotator in annotators:
        print("no encoding", test_annotator)
        evaluate(model, test_samples, test_labels_anno[test_annotator])
    
    exit()
    
    # unaggregated
    for train_annotator in annotators:
        model = model_without_annotator()
        model = load_if_exists("no_encoding_" + train_annotator, model, train_samples, train_labels_anno[train_annotator], dev_samples, dev_labels_anno[train_annotator])
        print(train_annotator, "aggregated")
        evaluate(model, test_samples_all, test_labels_all)
        for test_annotator in annotators:
            print(train_annotator, test_annotator)
            evaluate(model, test_samples, test_labels_anno[test_annotator])

    # cross-validation
    for test_annotator in annotators:
        model = model_without_annotator()
        model = load_if_exists("no_encoding_cv_" + test_annotator, model, train_samples_all_except[test_annotator], train_labels_all_except[test_annotator], dev_samples_all_except[test_annotator], dev_labels_all_except[test_annotator])
        print("no encoding", "cv", test_annotator)
        evaluate(model, test_samples, test_labels_anno[test_annotator])
    
    # one-hot
    anno_units = 6
    model = model_with_annotator(anno_units)
    model = load_if_exists("one-hot", model, [train_samples_all, train_annos_all], train_labels_all, [dev_samples_all, dev_annos_all], dev_labels_all)
    print("one-hot", "aggregated", "train", "with annotator")
    evaluate(model, [train_samples_all, train_annos_all], train_labels_all)
    print("one-hot", "aggregated", "dev", "with annotator")
    evaluate(model, [dev_samples_all, dev_annos_all], dev_labels_all)
    print("one-hot", "aggregated", "test", "with annotator")
    evaluate(model, [test_samples_all, test_annos_all], test_labels_all)
    for test_annotator in annotators:
        print("one-hot", test_annotator, "with annotator")
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
    print("one-hot", "aggregated", "w/o annotator")
    evaluate(model, [test_samples_all, test_annos_all_special], test_labels_all)
    for test_annotator in annotators:
        print("one-hot", test_annotator, "w/o annotator")
        evaluate(model, [test_samples, test_annos_special], test_labels_anno[test_annotator])
    
    feats = None # (all) questionnaire features
    train_samples, train_samples_all, train_samples_all_except, train_samples_same, train_annotators_all, train_annotators_all_except, train_annos_all, train_annos_all_except, train_annos_all_special, train_annos_all_special_same, train_annos_same, train_annos_special, train_annos_anno, train_labels_all, train_labels_all_except, train_labels_same, train_labels_anno = transform_data(train_samples, train_labels, feats)
    dev_samples, dev_samples_all, dev_samples_all_except, dev_samples_same, dev_annotators_all, dev_annotators_all_except, dev_annos_all, dev_annos_all_except, dev_annos_all_special, dev_annos_all_special_same, dev_annos_same, dev_annos_special, dev_annos_anno, dev_labels_all, dev_labels_all_except, dev_labels_same, dev_labels_anno = transform_data(dev_samples, dev_labels, feats)
    test_samples, test_samples_all, test_samples_all_except, test_samples_same, test_annotators_all, test_annotators_all_except, test_annos_all, test_annos_all_except, test_annos_all_special, test_annos_all_special_same, test_annos_same, test_annos_special, test_annos_anno, test_labels_all, test_labels_all_except, test_labels_same, test_labels_anno = transform_data(test_samples, test_labels, feats)

    names = train_annotators_all[:6]
    names.append("AX")

    # cluster annotators by full questionnaire
    vectors = list(train_annos_all)[:6]
    vectors.append(list(train_annos_special)[0])
    X = np.array(vectors)
    linked = linkage(X, 'average')
    plt.figure(figsize=(10, 7))
    dendrogram(linked, orientation='top', labels=names, distance_sort='descending', color_threshold=0)
    plt.savefig("figs/fig2.png")

    # cluster annotators by full questionnaire after SVD to 6 dimensions
    vectors = list(train_annos_all)[:6]
    svd = TruncatedSVD(n_components=6, n_iter=5, random_state=42)
    vectors = list(svd.fit_transform(vectors))
    vectors.append(np.zeros(6, dtype=np.float32))
    X = np.array(vectors)
    linked = linkage(X, 'average')
    plt.figure(figsize=(10, 7))
    dendrogram(linked, orientation='top', labels=names, distance_sort='descending', color_threshold=0)
    plt.savefig("figs/fig1.png")

    anno_units = 94+14
    
    # questionnaire
    model = model_with_annotator(anno_units)
    model = load_if_exists("questionnaire", model, [train_samples_all, train_annos_all], train_labels_all, [dev_samples_all, dev_annos_all], dev_labels_all)
    print("questionnaire", "aggregated", "with annotator")
    evaluate(model, [test_samples_all, test_annos_all], test_labels_all)
    for test_annotator in annotators:
        print("questionnaire", test_annotator, "with annotator")
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
    print("questionnaire", "aggregated", "w/o annotator")
    evaluate(model, [test_samples_all, test_annos_all_special], test_labels_all)
    for test_annotator in annotators:
        print("questionnaire", test_annotator, "w/o annotator")
        evaluate(model, [test_samples, test_annos_special], test_labels_anno[test_annotator])
    
    # cross-validation
    for test_annotator in annotators:
        model = model_with_annotator(anno_units)
        model = load_if_exists("questionnaire_cv_" + test_annotator, model, [train_samples_all_except[test_annotator], train_annos_all_except[test_annotator]], train_labels_all_except[test_annotator], [dev_samples_all_except[test_annotator], dev_annos_all_except[test_annotator]], dev_labels_all_except[test_annotator])
        print("questionnaire", "cv", test_annotator)
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
    
    feats = select_top_feats(annotators, 10)
    train_samples, train_samples_all, train_samples_all_except, train_samples_same, train_annotators_all, train_annotators_all_except, train_annos_all, train_annos_all_except, train_annos_all_special, train_annos_all_special_same, train_annos_same, train_annos_special, train_annos_anno, train_labels_all, train_labels_all_except, train_labels_same, train_labels_anno = transform_data(train_samples, train_labels, feats)
    dev_samples, dev_samples_all, dev_samples_all_except, dev_samples_same, dev_annotators_all, dev_annotators_all_except, dev_annos_all, dev_annos_all_except, dev_annos_all_special, dev_annos_all_special_same, dev_annos_same, dev_annos_special, dev_annos_anno, dev_labels_all, dev_labels_all_except, dev_labels_same, dev_labels_anno = transform_data(dev_samples, dev_labels, feats)
    test_samples, test_samples_all, test_samples_all_except, test_samples_same, test_annotators_all, test_annotators_all_except, test_annos_all, test_annos_all_except, test_annos_all_special, test_annos_all_special_same, test_annos_same, test_annos_special, test_annos_anno, test_labels_all, test_labels_all_except, test_labels_same, test_labels_anno = transform_data(test_samples, test_labels, feats)

    # cluster annotators by top-10 correlating features from questionnaire
    vectors = list(train_annos_all)[:6]
    vectors.append(list(train_annos_special)[0])
    X = np.array(vectors)
    linked = linkage(X, 'average')
    plt.figure(figsize=(10, 7))
    dendrogram(linked, orientation='top', labels=names, distance_sort='descending', color_threshold=0)
    plt.savefig("figs/fig3.png")

    anno_units = 10

    # selection
    model = model_with_annotator(anno_units)
    model = load_if_exists("selection", model, [train_samples_all, train_annos_all], train_labels_all, [dev_samples_all, dev_annos_all], dev_labels_all)
    print("selection", "aggregated", "with annotator")
    evaluate(model, [test_samples_all, test_annos_all], test_labels_all)
    for test_annotator in annotators:
        print("selection", test_annotator, "with annotator")
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
    print("selection", "aggregated", "w/o annotator")
    evaluate(model, [test_samples_all, test_annos_all_special], test_labels_all)
    for test_annotator in annotators:
        print("selection", test_annotator, "w/o annotator")
        evaluate(model, [test_samples, test_annos_special], test_labels_anno[test_annotator])
    
    # cross-validation
    for test_annotator in annotators:
        model = model_with_annotator(anno_units)
        model = load_if_exists("selection_cv_" + test_annotator, model, [train_samples_all_except[test_annotator], train_annos_all_except[test_annotator]], train_labels_all_except[test_annotator], [dev_samples_all_except[test_annotator], dev_annos_all_except[test_annotator]], dev_labels_all_except[test_annotator])
        print("selection", "cv", test_annotator)
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
    
    # cross-validation (automatic selection)
    for test_annotator in annotators:
        feats = select_top_feats([annotator for annotator in annotators if annotator != test_annotator], 10)
        train_samples, train_samples_all, train_samples_all_except, train_samples_same, train_annotators_all, train_annotators_all_except, train_annos_all, train_annos_all_except, train_annos_all_special, train_annos_all_special_same, train_annos_same, train_annos_special, train_annos_anno, train_labels_all, train_labels_all_except, train_labels_same, train_labels_anno = transform_data(train_samples, train_labels, feats)
        dev_samples, dev_samples_all, dev_samples_all_except, dev_samples_same, dev_annotators_all, dev_annotators_all_except, dev_annos_all, dev_annos_all_except, dev_annos_all_special, dev_annos_all_special_same, dev_annos_same, dev_annos_special, dev_annos_anno, dev_labels_all, dev_labels_all_except, dev_labels_same, dev_labels_anno = transform_data(dev_samples, dev_labels, feats)
        test_samples, test_samples_all, test_samples_all_except, test_samples_same, test_annotators_all, test_annotators_all_except, test_annos_all, test_annos_all_except, test_annos_all_special, test_annos_all_special_same, test_annos_same, test_annos_special, test_annos_anno, test_labels_all, test_labels_all_except, test_labels_same, test_labels_anno = transform_data(test_samples, test_labels, feats)
        
        vectors = list(train_annos_all)[:6]
        vectors.append(list(train_annos_special)[0])
        X = np.array(vectors)
        linked = linkage(X, 'average')
        plt.figure(figsize=(10, 7))
        dendrogram(linked, orientation='top', labels=names, distance_sort='descending', color_threshold=0)
        plt.savefig("figs/fig3_" + test_annotator + ".png")
        
        model = model_with_annotator(anno_units)
        model = load_if_exists("selection_cv_" + test_annotator + "_auto", model, [train_samples_all_except[test_annotator], train_annos_all_except[test_annotator]], train_labels_all_except[test_annotator], [dev_samples_all_except[test_annotator], dev_annos_all_except[test_annotator]], dev_labels_all_except[test_annotator])
        print("selection", "cv", test_annotator, "auto")
        evaluate(model, [test_samples, test_annos_anno[test_annotator]], test_labels_anno[test_annotator])
