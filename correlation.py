from scipy.stats import pearsonr

alphas = {
    "A1" : [1.00, 0.68, 0.64, 0.90, 0.56, 0.67],
    "A2" : [0.68, 1.00, 0.62, 0.67, 0.62, 0.72],
    "A3" : [0.64, 0.62, 1.00, 0.63, 0.73, 0.60],
    "A4" : [0.90, 0.67, 0.63, 1.00, 0.58, 0.68],
    "A5" : [0.56, 0.62, 0.73, 0.58, 1.00, 0.50],
    "A6" : [0.67, 0.72, 0.60, 0.68, 0.50, 1.00]
}

accuracies = {
    "A1" : [0.87, 0.78, 0.76, 0.85, 0.76, 0.72],
    "A2" : [0.77, 0.87, 0.77, 0.75, 0.76, 0.79],
    "A3" : [0.75, 0.78, 0.86, 0.77, 0.86, 0.77],
    "A4" : [0.85, 0.77, 0.75, 0.86, 0.78, 0.75],
    "A5" : [0.77, 0.80, 0.81, 0.78, 0.87, 0.74],
    "A6" : [0.78, 0.83, 0.78, 0.79, 0.76, 0.88]
}

data1 = []
data2 = []
for annotator in sorted(alphas.keys()):
    data1 += alphas[annotator]
    data2 += accuracies[annotator]
print(pearsonr(data1, data2))