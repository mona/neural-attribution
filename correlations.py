import numpy as np
import os
import random
import pandas as pd
from itertools import chain, combinations
from scipy.stats import pearsonr
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import VarianceThreshold, chi2, f_classif, mutual_info_classif
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder, MultiLabelBinarizer

from vectorizer import read_samples


def select_top_feats(annotators, n):
    """Select top-n correlating features on the training set.

    Args:
        annotators (list of str): Annotators to include in the calculations.
        n (int): Number of features to select.
    
    Returns:
        list of str: List of feature names.
    
    """
    correlations = get_correlations(annotators)
    correlations = sorted([(col, r) for label in correlations for col, r, p, v in correlations[label]], key=lambda tup: abs(tup[1]), reverse=True)
    feats = []
    while len(feats) < n and len(correlations) > 0:
        col, r = correlations.pop(0)
        if col not in feats:
            feats.append(col)
    return feats


def get_correlations(annotators, var_min=0.8):
    """Calculate correlations between labels and features on the training set.

    Args:
        annotators (list of str): Annotators to include in the calculations.
        var_min (float): Minimum variance of the features to include.
    
    Returns:
        (dict of str:(list of (str,float,float,float))): Dictionary which maps labels to list of tuples.
            Each tuple has the format (feature name, pearson's coefficient, p-value, variance).
    
    """
    questionnaire = pd.read_csv(os.path.join(os.path.dirname(__file__), "annotators", "questionnaire.csv"), index_col=0)
    samples = read_samples("train")
    clauses = set()
    clause_labels = {}
    for sample in samples:
        labels = sample["labels"]
        text = sample["text"]
        for clause in labels:
            clauses.add((text, clause))
            clause_labels[(text, clause)] = labels[clause]
    clauses = sorted(list(clauses), key=lambda tup: str(tup))
    random.Random(0).shuffle(clauses)
    data_i = {}
    data_o = {}
    indices = set()
    for i in range(0, len(clauses)-(len(clauses)%100), 100):
        batch = clauses[i:i+100]
        for annotator in annotators:
            index = str(i) + ":" + annotator
            indices.add(index)
            data_i[index] = {}
            data_o[index] = {}
            for col in questionnaire:
                data_i[index][col] = questionnaire[col][annotator]
            for label in ["Figur", "Erzählinstanz", "Verdacht Autor"]:
                data_o[index][label] = 0
            for clause in batch:
                labels = clause_labels[clause]
                for label in labels[annotator]:
                    data_o[index][label] += 1
    indices = sorted(list(indices))
    correlations = {}
    for col in questionnaire:
        x = [data_i[index][col] for index in indices]
        v = np.var(questionnaire[col])
        if v > var_min:
            for label in ["Figur", "Erzählinstanz", "Verdacht Autor"]:
                y = [data_o[index][label] for index in indices]
                r, p = pearsonr(x, y)
                if not np.isnan(r):
                    try:
                        correlations[label].append((col, r, p, v))
                    except:
                        correlations[label] = [(col, r, p, v)]
    for label in correlations:
        correlations[label] = sorted(correlations[label], key=lambda tup: abs(tup[1]), reverse=True)
    return correlations

if __name__ == "__main__":
    annotators = ["A1", "A2", "A3", "A4", "A5", "A6"]
    correlations = get_correlations(annotators)
    for label in correlations:
        for col, r, p, v in correlations[label]:
            if abs(r) >= 0.5:
                print(label, col, r, p, v)
    print(select_top_feats(annotators, 10))
    for except_annotator in annotators:
        print(except_annotator, select_top_feats([annotator for annotator in annotators if annotator != except_annotator], 10))