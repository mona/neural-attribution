# Neural Attribution

This repository contains the data and scripts to reproduce the results for an [article](https://www.frontiersin.org/articles/10.3389/frai.2021.725321/) published at Frontiers. See release [v1](https://gitlab.gwdg.de/mona/neural-attribution/-/releases/v1) for the version as used for the Frontiers paper (the current state of the repository is work in progress).

## Set-up

You can clone the repository, create a virtual environment and install the requirements using the command line as follows:

```sh
git clone https://gitlab.gwdg.de/mona/neural-attribution.git
cd neural-attribution
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Data

The data for the experiments was extracted from the public corpus [MONACO](https://gitlab.gwdg.de/mona/korpus-public) ([v1.1](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v1.1)). The samples for training, development and test set are saved as JSON files in `data`.

The scripts which need vectorized samples, call the vectorizer. Vectorizing the samples can take some time, hence the vectorized damples are saved as Pickle file in `data` after the first computation; in subsequent function calls, the vectors will be loaded instead of recalculated.

The answers from the questionnaire are saved in `annotators`.

## Models

Pre-trained models for all experiments are saved in `models`. These models are loaded in the experiment scripts. If you want to retrain them, delete the models from `models` or move them to another directory.

## Experiments

Run the questionnaire correlation experiments:

```sh
python correlations.py
```

Run the attribution classification experiments:

```sh
python main.py
```

Compute the inter-annotator-agreement matrix:

```sh
python agreements.py
```

Compute the correlation between agreement and accuracy:

```sh
python correlation.py
```

Inspect sample-wise predictions for the test set:

```sh
python predictions.py
```

## Lincence/Citation

This work is licensed under a Creative Commons Attribution (CC-BY) 4.0 International License.

If you use any of this code, you should cite our corresponding article:

> Tillmann Dönicke, Hanna Varachkina, Anna Mareike Weimer, Luisa Gödeke, Florian Barth, Benjamin Gittel, Anke Holler, and Caroline Sporleder (2022). "Modelling Speaker Attribution in Narrative Texts With Biased and Bias-Adjustable Neural Networks". URL: https://www.frontiersin.org/articles/10.3389/frai.2021.725321/

The texts in our data are partially affected by other licenses. See [here](https://gitlab.gwdg.de/mona/korpus-public#lincencecitation) for more information.
