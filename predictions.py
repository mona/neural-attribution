import keras
import numpy as np
import os
from vectorizer import LABEL_ENCODER, read_samples, vectorize_sample, pad_sample, prepare

path = os.path.join("models", "no_encoding")
model = keras.models.load_model(path)

samples = read_samples("test")

for sample in samples:
    id = sorted(sample["labels"].keys())[0]
    print(id)
    print(sample["original"])
    print(sample["tokens"])
    clause_embeddings, clause_labels = vectorize_sample(sample)
    for i, clause_embedding in enumerate(clause_embeddings):
        X = prepare([pad_sample(clause_embedding, 123)])
        for annotator in sorted(sample["labels"][id]):
            gold_labels = clause_labels[i][annotator]
            print(annotator, LABEL_ENCODER.inverse_transform(np.asarray([gold_labels]))[0])
        pred_labels = [round(x) for x in model.predict(X)[0]]
        print("AX", LABEL_ENCODER.inverse_transform(np.asarray([pred_labels]))[0])
    input()