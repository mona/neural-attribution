import tensorflow as tf
from tensorflow.keras import models, layers, optimizers, regularizers


BERT_DIMENSIONS = 768

LSTM_UNITS = 20
LSTM_DROPOUT = 0.2

HIDDEN_UNITS = 20
HIDDEN_L2 = 0.0001
HIDDEN_ACTIVATION = "relu"

OUTPUT_UNITS = 3
OUTPUT_ACTIVATION = "sigmoid"

LOSS = "binary_crossentropy"
METRIC = "binary_accuracy"
OPTIMIZER = optimizers.Adam(learning_rate=0.0001)


def model_without_annotator():
    """Model without annotator representation.

    Return:
        `Sequential`: Keras model.
    
    """
    model = models.Sequential()
    model.add(layers.Input(shape=(None,1+BERT_DIMENSIONS)))
    model.add(layers.Bidirectional(layers.LSTM(units=LSTM_UNITS, return_sequences=False, dropout=LSTM_DROPOUT)))
    model.add(layers.Dense(units=HIDDEN_UNITS, activation=HIDDEN_ACTIVATION, activity_regularizer=regularizers.l2(HIDDEN_L2)))
    model.add(layers.Dense(units=OUTPUT_UNITS, activation=OUTPUT_ACTIVATION))
    model.compile(loss=LOSS, metrics = [METRIC], optimizer=OPTIMIZER)
    return model


def model_with_annotator(anno_units):
    """Model with annotator representation.

    Args:
        anno_units (int): Number of dimensions in the annotator representations.
    
    Return:
        `Sequential`: Keras model.
    
    """
    input1 = tf.keras.Input(shape=(None, 1+BERT_DIMENSIONS))
    input2 = tf.keras.Input(shape=(anno_units,))

    x = tf.keras.layers.Bidirectional(layers.LSTM(units=LSTM_UNITS, return_sequences=False, dropout=LSTM_DROPOUT))(input1)
    x = tf.keras.layers.concatenate((x, input2))
    x = tf.keras.layers.Dense(units=HIDDEN_UNITS, activation=HIDDEN_ACTIVATION, activity_regularizer=regularizers.l2(HIDDEN_L2))(x)
    x = tf.keras.layers.Dense(units=OUTPUT_UNITS, activation=OUTPUT_ACTIVATION)(x)

    model = tf.keras.Model(inputs=(input1, input2), outputs=x)
    model.compile(loss=LOSS, metrics = [METRIC], optimizer=OPTIMIZER)
    return model