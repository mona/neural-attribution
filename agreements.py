from nltk.metrics.agreement import AnnotationTask
from nltk.metrics import masi_distance

from vectorizer import gimme_samples, transform_data

if __name__ == "__main__":
    train_samples, dev_samples, test_samples, train_labels, dev_labels, test_labels = gimme_samples()

    feats = []
    train_samples, train_samples_all, train_samples_all_except, train_samples_same, train_annotators_all, train_annotators_all_except, train_annos_all, train_annos_all_except, train_annos_all_special, train_annos_all_special_same, train_annos_same, train_annos_special, train_annos_anno, train_labels_all, train_labels_all_except, train_labels_same, train_labels_anno = transform_data(train_samples, train_labels, feats)
    
    annotators = sorted(list(set(train_annotators_all)))

    for annotator1 in annotators:
        row = []
        x1 = train_labels_anno[annotator1]
        x1 = [(annotator1, str(i), frozenset([str(j) for j, v in enumerate(x) if v >= 0.5])) for i, x in enumerate(x1)]
        for annotator2 in annotators:
            x2 = train_labels_anno[annotator2]
            x2 = [(annotator2, str(i), frozenset([str(j) for j, v in enumerate(x) if v >= 0.5])) for i, x in enumerate(x2)]
            task = AnnotationTask(distance=masi_distance)
            task.load_array(x1+x2)
            row.append(task.alpha())
        print(annotator1, row)